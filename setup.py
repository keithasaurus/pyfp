from setuptools import setup

setup(
    name='pyfp',  # Required
    version='0.0.1',  # Required
    packages=['pyfp'],
    package_data={"pyfp": ["py.typed"]},
)
