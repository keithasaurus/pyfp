#!/usr/bin/env bash

set -e

mypy pyfp tests examples
pytest
flake8

